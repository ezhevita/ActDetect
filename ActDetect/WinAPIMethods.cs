﻿using System;
using System.ComponentModel;
using System.Runtime.InteropServices;
using System.Threading;
using System.Threading.Tasks;
using ActDetect.Events;

namespace ActDetect {
	internal static class WinAPIMethods {
		#region Disposing

		internal static CancellationTokenSource TokenSource;

		#endregion

		#region Initialization

		internal static bool IsInitCalled;

		internal static void InitActiveWindowEvent() {
			IsInitCalled = true;
			WinEventProcess = ActiveWindowEvent.ProcessEvent;
			TokenSource = new CancellationTokenSource();

			Task.Factory.StartNew(() => SetAndProcessHook(TokenSource.Token));
		}

		#endregion

		#region WinAPI functions arguments constants

		private const int WinEventOutOfContext = 0x0;
		internal const int EventSystemForeground = 0x3;

		#endregion

		#region External WinAPI methods

		[DllImport("user32.dll")]
		private static extern IntPtr DispatchMessage(ref Msg lpMsg);

		[DllImport("user32.dll")]
		private static extern IntPtr GetForegroundWindow();

		[DllImport("user32.dll")]
		private static extern bool GetLastInputInfo(ref LastInputInfo plii);

		[DllImport("user32.dll", SetLastError = true)]
		private static extern bool GetMessage(out Msg lpMsg, IntPtr hWnd, uint wMsgFilterMin, uint wMsgFilterMax);

		[DllImport("user32.dll")]
		internal static extern IntPtr GetWindowThreadProcessId(IntPtr hWnd, out uint processId);

		[DllImport("user32.dll", SetLastError = true)]
		private static extern IntPtr SetWinEventHook(int eventMin, int eventMax, IntPtr hmodWinEventProc, WinEventDelegate lpfnWinEventProc, int idProcess, int idThread, int dwflags);

		[DllImport("user32.dll")]
		private static extern bool TranslateMessage(ref Msg lpMsg);

		[DllImport("user32.dll")]
		private static extern bool UnhookWinEvent(IntPtr hWinEventHook);

		#endregion

		#region Structures

		[StructLayout(LayoutKind.Sequential)]
		private struct Msg {
			private readonly IntPtr HWnd;
			private readonly uint Message;
			private readonly IntPtr WParam;
			private readonly IntPtr LParam;
			private readonly uint Time;
		}

		[StructLayout(LayoutKind.Sequential)]
		private struct LastInputInfo {
			internal uint StructureSize;
			internal readonly uint MillisecondsTillLastInput;
		}

		#endregion

		#region Methods to work with WinAPI calls

		internal static ulong GetIdleTime() {
			LastInputInfo lastInput = new LastInputInfo();
			lastInput.StructureSize = (uint) Marshal.SizeOf(lastInput);
			GetLastInputInfo(ref lastInput);

			return (uint) (Environment.TickCount - lastInput.MillisecondsTillLastInput);
		}

		internal static void SendFirstProcessInfo() {
			IntPtr hWnd = GetForegroundWindow();
			WinEventProcess(IntPtr.Zero, EventSystemForeground, hWnd, 0, 0, 0, 0);
		}

		private static void SetAndProcessHook(CancellationToken token) {
			IntPtr hook = SetWinEventHook(EventSystemForeground, EventSystemForeground, IntPtr.Zero, WinEventProcess, 0, 0, WinEventOutOfContext);
			if (hook == IntPtr.Zero) {
				throw new Win32Exception(Marshal.GetLastWin32Error());
			}

			while (!token.IsCancellationRequested) {
				if (GetMessage(out Msg msg, IntPtr.Zero, 0, 0)) {
					TranslateMessage(ref msg);
					DispatchMessage(ref msg);
				}
			}

			UnhookWinEvent(hook);
		}

		#endregion

		#region Events

		private static WinEventDelegate WinEventProcess;

		private delegate void WinEventDelegate(IntPtr hWinEventHook, uint eventType, IntPtr hWnd, int idObject, int idChild, uint dwEventThread, uint dwmsEventTime);

		#endregion
	}
}
