﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace ActDetect.Events {
	public class ActiveStateEvent {
		/// <summary>
		///  Event to track user's active state
		/// </summary>
		/// <param name="sender">Object that triggered this event</param>
		/// <param name="e">Changed user's active state</param>
		public delegate void ActiveStateEventHandler(object sender, ActiveStateEventArgs e);

		private readonly CancellationTokenSource CancellationTokenSource = new CancellationTokenSource();
		private readonly TimeSpan InactivityPeriod;

		private ActiveStateEventHandler BackingActiveStateChanged;

		private ActiveState LastActiveState = ActiveState.Active;

		/// <summary>
		///  Create new instance of event for detecting active state
		/// </summary>
		/// <param name="inactivityPeriod">Period of inactivity after which event will be triggered</param>
		public ActiveStateEvent(TimeSpan inactivityPeriod) {
			InactivityPeriod = inactivityPeriod;
			Task.Factory.StartNew(() => ProcessEvent(CancellationTokenSource.Token));
		}

		private async void ProcessEvent(CancellationToken token) {
			while (true) {
				if (token.IsCancellationRequested) {
					return;
				}

				TimeSpan idleTime = TimeSpan.FromMilliseconds(WinAPIMethods.GetIdleTime());
				switch (LastActiveState) {
					case ActiveState.Active when idleTime >= InactivityPeriod:
						BackingActiveStateChanged(this, new ActiveStateEventArgs(ActiveState.NotActive));
						LastActiveState = ActiveState.NotActive;
						break;
					case ActiveState.Active:
						await Task.Delay(InactivityPeriod - idleTime, token).ConfigureAwait(false);
						break;
					case ActiveState.NotActive when idleTime < InactivityPeriod:
						BackingActiveStateChanged(this, new ActiveStateEventArgs(ActiveState.Active));
						LastActiveState = ActiveState.Active;

						await Task.Delay(InactivityPeriod, token).ConfigureAwait(false);
						break;
				}
			}
		}

		public event ActiveStateEventHandler ActiveStateChanged {
			add => BackingActiveStateChanged += value;

			// ReSharper disable once DelegateSubtraction
			remove => BackingActiveStateChanged -= value;
		}

		~ActiveStateEvent() {
			CancellationTokenSource.Cancel();
			BackingActiveStateChanged = null;
		}

		public class ActiveStateEventArgs : EventArgs {
			public readonly ActiveState ActiveState;

			internal ActiveStateEventArgs(ActiveState activeState) => ActiveState = activeState;
		}
		
		public enum ActiveState {
			Active,
			NotActive
		}
	}
}
