﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using JetBrains.Annotations;

// ReSharper disable once DelegateSubtraction

namespace ActDetect.Events {
	public class ActiveWindowEvent {
		/// <summary>
		///  Event to track user's active window
		/// </summary>
		/// <param name="sender">Object that triggered this event</param>
		/// <param name="e">Changed active window information</param>
		public delegate void ActiveWindowEventHandler(object sender, ActiveWindowEventArgs e);

		private static readonly List<ActiveWindowEvent> RegisteredEvents = new List<ActiveWindowEvent>();

		private readonly IReadOnlyCollection<string> FilterProcessList;

		private ActiveWindowEventHandler BackingActiveWindowChanged;

		private bool FirstProcessSent;
		private uint LastProcessID;

		/// <summary>
		///  Create new instance of event for detecting user's active window
		/// </summary>
		[PublicAPI]
		public ActiveWindowEvent() {
			FilterProcessList = null;
			if (!WinAPIMethods.IsInitCalled) {
				WinAPIMethods.InitActiveWindowEvent();
			}

			RegisteredEvents.Add(this);
		}

		/// <summary>
		///  Create new instance of event for detecting user's active window
		/// </summary>
		/// <param name="ignoreProcessList">List of processes, which should be ignored</param>
		[PublicAPI]
		public ActiveWindowEvent(IReadOnlyCollection<string> ignoreProcessList) {
			FilterProcessList = ignoreProcessList;
			if (!WinAPIMethods.IsInitCalled) {
				WinAPIMethods.InitActiveWindowEvent();
			}

			RegisteredEvents.Add(this);
		}

		internal static void ProcessEvent(IntPtr hWinEventHook, uint eventType, IntPtr hWnd, int idObject, int idChild, uint dwEventThread, uint dwmsEventTime) {
			if ((eventType != WinAPIMethods.EventSystemForeground) || (idObject != 0) || (idChild != 0)) {
				return;
			}

			WinAPIMethods.GetWindowThreadProcessId(hWnd, out uint processId);
			if (processId == 0) {
				return;
			}

			RegisteredEvents.ForEach(@event => @event.ProcessEvent(processId));
		}

		private void ProcessEvent(uint processId) {
			if (LastProcessID == processId) {
				return;
			}

			Process activeProcess;
			try {
				activeProcess = Process.GetProcessById((int) processId);
			} catch (ArgumentException) {
				return;
			}

			if (FilterProcessList?.Contains(activeProcess.ProcessName) != true) {
				LastProcessID = processId;
				BackingActiveWindowChanged(this, new ActiveWindowEventArgs(activeProcess));
			}
		}

		[PublicAPI]
		public event ActiveWindowEventHandler ActiveWindowChanged {
			add {
				BackingActiveWindowChanged += value;

				if (!FirstProcessSent) {
					WinAPIMethods.SendFirstProcessInfo();
					FirstProcessSent = true;
				}
			}
			remove => BackingActiveWindowChanged -= value;
		}

		~ActiveWindowEvent() {
			BackingActiveWindowChanged = null;
			WinAPIMethods.TokenSource.Cancel();
		}

		[PublicAPI]
		public class ActiveWindowEventArgs : EventArgs {
			public readonly Process ActiveWindowProcess;

			internal ActiveWindowEventArgs(Process process) => ActiveWindowProcess = process;
		}
	}
}
