﻿using System;
using System.Collections.Generic;
using System.Threading;
using ActDetect.Events;

namespace ActDetectExample {
	internal static class Program {
		private static void Main() {
			ActiveStateEvent stateDetect = new ActiveStateEvent(TimeSpan.FromSeconds(5));
			ActiveWindowEvent windowDetect = new ActiveWindowEvent(new List<string> {
				"explorer"
			});
			stateDetect.ActiveStateChanged += StateDetectOnActiveStateChanged;
			windowDetect.ActiveWindowChanged += WindowDetectOnActiveWindowChanged;
			Console.WriteLine("Started");
			Thread.CurrentThread.Join();
		}

		private static void WindowDetectOnActiveWindowChanged(object sender, ActiveWindowEvent.ActiveWindowEventArgs e) {
			Console.WriteLine("Detected window: " + e.ActiveWindowProcess.MainModule?.FileVersionInfo.FileDescription);
		}

		private static void StateDetectOnActiveStateChanged(object sender, ActiveStateEvent.ActiveStateEventArgs e) {
			Console.WriteLine("Detected state: " + e.ActiveState);
		}
	}
}
